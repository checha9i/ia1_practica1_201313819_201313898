import React, { Component } from 'react';

import './Estudiante.css';
import { Row, Col, Form, Label, Input, Button, FormGroup, FormFeedback, FormText } from 'reactstrap';

class ConsultaEstudiante extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anio: "",
      carnet: "",
      semestre: ""
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeAnio = this.onChangeAnio.bind(this);
    this.onChangeCarnet = this.onChangeCarnet.bind(this);
    this.onChangeSemestre = this.onChangeSemestre.bind(this);
    this.reset = this.reset.bind(this);
  }
  reset() {
    this.setState({
      anio: "",
      carnet: "",
      semestre: ""
    });
  }

  onChangeAnio(e) {
    this.setState({ anio: e.target.value });
  }
  onChangeCarnet(e) {
    this.setState({ carnet: e.target.value });
  }
  onChangeSemestre(e) {
    this.setState({ semestre: e.target.value });
  }


  handleSubmit(e) {
    e.preventDefault()
    this.props.SearchByStudent([this.state.anio, this.state.carnet, this.state.semestre]);
    this.reset()
  }

  render() {
    return (
      <Col md={6}>
        <h2 className="offset-3">Consulta de Estudiante</h2>
        <Row>
          <Col md={8} className="offset-md-2">
            <Form onSubmit={this.handleSubmit}>
              <FormGroup row className="offset-md-2">
                <Label for="carnet">Carné</Label>
                <Input type="text" name="carnet" id="carnet" placeholder="Carné" value={this.state.carnet} onChange={this.onChangeCarnet} />
              </FormGroup>
              <FormGroup row className="offset-md-2">
                <Label for="año">Año</Label>
                <Input type="text" name="año" id="año" placeholder="Año" value={this.state.anio} onChange={this.onChangeAnio} />
              </FormGroup>
              <FormGroup row className="offset-md-2">
                <Label for="semestre">Semestre</Label>
                <Input type="text" name="semestre" id="semestre" placeholder="Semestre" value={this.state.semestre} onChange={this.onChangeSemestre} />
              </FormGroup>
              <Button outline color="primary" className="offset-md-2" type="submit">
                <span className="fa fa-search"></span> Consultar
            </Button>
            </Form>
          </Col>
        </Row>
      </Col>
    );
  }
}

class InsertarEstudiante extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anio: "",
      carnet: "",
      semestre: "",
      dpi: "",
      nombre: "",
      grupo: "",
      correo: ""
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeAnio = this.onChangeAnio.bind(this);
    this.onChangeCarnet = this.onChangeCarnet.bind(this);
    this.onChangeSemestre = this.onChangeSemestre.bind(this);
    this.onChangeGrupo = this.onChangeGrupo.bind(this);
    this.onChangeDPI = this.onChangeDPI.bind(this);
    this.onChangeNombre = this.onChangeNombre.bind(this);
    this.onChangeCorreo = this.onChangeCorreo.bind(this);
    this.reset = this.reset.bind(this);
  }

  reset() {
    this.setState({
      anio: "",
      carnet: "",
      semestre: "",
      dpi: "",
      nombre: "",
      grupo: "",
      correo: ""
    });
  }

  onChangeAnio(e) {
    this.setState({ anio: e.target.value });
  }
  onChangeCarnet(e) {
    this.setState({ carnet: e.target.value });
  }
  onChangeSemestre(e) {
    this.setState({ semestre: e.target.value });
  }
  onChangeNombre(e) {
    this.setState({ nombre: e.target.value });
  }
  onChangeCorreo(e) {
    this.setState({ correo: e.target.value });
  }
  onChangeGrupo(e) {
    this.setState({ grupo: e.target.value });
  }

  onChangeDPI(e) {
    this.setState({ dpi: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();

    let student = {
      anio: this.state.anio,
      carnet: this.state.carnet,
      semestre: this.state.semestre,
      nombre: this.state.nombre,
      grupo: this.state.grupo,
      dpi: this.state.dpi,
      correo: this.state.correo
    }
    this.props.onInsert(student);
    //this.reset()
    alert("Se ingreso el alumno: " + student.nombre)

  }

  render() {
    return (
      <Col md={6}>
        <h2>Insertar Estudiante</h2>
        <Row>
          <Col md={8}>
            <Form onSubmit={this.handleSubmit}>
              <FormGroup row className="offset-md-2" >
                <Label for="nombre">Nombre</Label>
                <Input type="text" name="nombre" id="nombre" placeholder="Nombre" value={this.state.nombre} onChange={this.onChangeNombre} required />
                <FormFeedback onInvalid>Ingrese un nombre</FormFeedback>

              </FormGroup>
              <FormGroup row className="offset-md-2">
                <Label for="carnet">Carné</Label>
                <Input type="text" name="carnet" id="carnet" placeholder="Carné" value={this.state.carnet} onChange={this.onChangeCarnet} required />
              </FormGroup>
              <FormGroup row className="offset-md-2">
                <Label for="dpi">DPI</Label>
                <Input type="text" name="dpi" id="dpi" placeholder="DPI" value={this.state.dpi} onChange={this.onChangeDPI} required />
              </FormGroup>
              <FormGroup row className="offset-md-2">
                <Label for="semestre">Semestre</Label>
                <Input type="text" name="semestre" id="semestre" placeholder="Semestre" value={this.state.semestre} onChange={this.onChangeSemestre} required />
              </FormGroup>
              <FormGroup row className="offset-md-2">
                <Label for="año">Año</Label>
                <Input type="text" name="año" id="año" placeholder="Año" value={this.state.anio} onChange={this.onChangeAnio} required />
              </FormGroup>
              <FormGroup row className="offset-md-2">
                <Label for="grupo">Grupo</Label>
                <Input type="text" name="grupo" id="grupo" placeholder="Grupo" value={this.state.grupo} onChange={this.onChangeGrupo} required />
              </FormGroup>
              <FormGroup row className="offset-md-2">
                <Label for="correo">Correo</Label>
                <Input type="email" name="correo" id="correo" placeholder="Correo" value={this.state.correo} onChange={this.onChangeCorreo} required />
              </FormGroup>
              <Button outline color="success" className="offset-md-2" type="submit">
                <span className="fa fa-plus"></span> Insertar
            </Button>
            </Form>
          </Col>
        </Row>
      </Col>
    );
  }
}

const Estudiante = (props) => (
  <div className="Estudiante">
    <Row>
      <ConsultaEstudiante SearchByStudent={props.SearchByStudent} />
      <InsertarEstudiante onInsert={props.onInsert} />
    </Row>
  </div>
);


export default Estudiante;
