import React, { Component } from "react";
import "./Main.css";
import { Container } from "reactstrap";
import Estudiante from "../Estudiante/Estudiante";
import Grupo from "../Grupo/Grupo";
import TablaEstudiantes from "../TablaEstudiantes/TablaEstudiantes";
import axios from "axios";
import url from "../../url";
import { Button } from "reactstrap";
class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      estudiantes: [],
      filter: false,
      tabla: [],
      student: {},
      grupo: "",
    };
    this.SearchByGroup = this.SearchByGroup.bind(this);
    this.SearchByStudent = this.SearchByStudent.bind(this);
    this.onInsert = this.onInsert.bind(this);
    this.limpiarFiltro = this.limpiarFiltro.bind(this);
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      axios.get(url + "/estudiantes", {}).then((response) => {
        // console.log(response.data)
        this.setState({
          estudiantes: response.data,
        });
      });

      if (!this.state.filter) {
        this.setState({
          tabla: this.state.estudiantes,
        });
      } else if (this.state.student.hasOwnProperty("grupo")) {
        this.setState({
          tabla: this.state.estudiantes.filter(
            (estudiante) => estudiante.Grupo === this.state.student.grupo
          ),
        });
      } else {
        this.setState({
          tabla: this.state.estudiantes.filter(
            (estudiante) =>
              parseInt(estudiante.Carnet) ===
                parseInt(this.state.student.carnet) &&
              parseInt(estudiante.anio) === parseInt(this.state.student.anio) &&
              estudiante.Semestre === this.state.student.semestre
          ),
        });
        if (this.state.tabla.length == 0) {
          this.setState({
            tabla: [
              {
                Nombre: '" "',
                Carnet: '" "',
              },
            ],
          });
        }
      }
    }, 3000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  SearchByStudent(student) {
    this.setState({
      filter: true,
      student: {
        carnet: student[1],
        anio: student[0],
        semestre: student[2],
      },
    });
    //console.log(this.state.student);
  }

  SearchByGroup(group) {
    this.setState({
      filter: true,
      student: {
        grupo: group,
      },
    });
    //console.log(this.state.student);
  }

  onInsert(e) {
    let data = {
      Nombre: e.nombre,
      Carnet: e.carnet,
      anio: e.anio,
      DPI: e.dpi,
      Semestre: e.semestre,
      Grupo: e.grupo,
      Correo: e.correo,
    };
    axios
      .post(url + "/estudiantes", data, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((response) => console.log(response));
  }

  limpiarFiltro(e) {
    e.preventDefault();
    this.setState({
      filter: false,
      student: {},
      grupo: "",
      tabla: this.state.estudiantes,
    });
  }

  render() {
    return (
      <div className="Main">
        <Container fluid>
          <Estudiante
            SearchByStudent={this.SearchByStudent}
            onInsert={this.onInsert}
          />
          <hr className="half-rule" />
          <Grupo SearchByGroup={this.SearchByGroup} />
          <hr className="half-rule" />
          <h1 className="offset-md-4">
            Listado de Estudiantes{" "}
            <Button outline color="warning" onClick={this.limpiarFiltro}>
              <span className="fa fa-trash-o"></span> Limpiar Filtro
            </Button>
          </h1>
          <TablaEstudiantes tabla={this.state.tabla} />
        </Container>
      </div>
    );
  }
}

export default Main;
