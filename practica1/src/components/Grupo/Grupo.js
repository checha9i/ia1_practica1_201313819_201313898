import React, { Component } from 'react';
import './Grupo.css';
import { Container, Col, Form, Label, Input, Button, FormGroup } from 'reactstrap';

class Grupo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      grupo: ""
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeGrupo = this.onChangeGrupo.bind(this);
    this.reset = this.reset.bind(this);
  }

  reset() {
    this.setState({
      grupo: ""
    });
  }

  onChangeGrupo(e) {
    this.setState({ grupo: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.SearchByGroup(this.state.grupo);
    this.reset()
  }

  render() {
    return (
      <Col>
        <Container>
          <h2 className="offset-4">Consulta de Estudiante</h2>
          <Form inline onSubmit={this.handleSubmit}>
            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Label for="grupo" className="mr-sm-2">Grupo</Label>
              <Input type="text" name="grupo" id="grupo" placeholder="Grupo" value={this.state.grupo} onChange={this.onChangeGrupo} required />
            </FormGroup>
            <Button outline color="info" type="submit">
              <span className="fa fa-search"></span> Consultar
            </Button>
          </Form>

        </Container>
      </Col>
    );
  }
}

export default Grupo;