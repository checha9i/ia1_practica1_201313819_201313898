import React from 'react';
import './TablaEstudiantes.css';
import { Table } from 'reactstrap';
const TablaEstudiantes = ({ tabla }) => (
  <Table striped responsive>
    <thead>
      <tr>
        <th>Carné</th>
        <th>Correo</th>
      </tr>
    </thead>
    <tbody>
      {tabla.map((estudiante, index) => {
        return (
          <tr key={index}>
            <td>{estudiante.Carnet}</td>
            <td>{estudiante.Correo}</td>
          </tr>
        );
      })}
    </tbody>
  </Table>
);

export default TablaEstudiantes;
