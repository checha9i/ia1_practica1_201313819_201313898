module.exports = {
  apps: [
    {
      name: 'api',
      cwd: '/home/ubuntu/api/ia1_practica1_201313819_201313898/api',
      script: 'npm',
      args: 'start',
      env: {
        NODE_ENV: 'production',
        DATABASE_HOST: 'http://100.27.14.6', // database Endpoint under 'Connectivity & Security' tab
        DATABASE_PORT: '27017',
        DATABASE_NAME: 'practica1', // DB name under 'Configuration' tab
        DATABASE_USERNAME: '', // default username
        DATABASE_PASSWORD: '',
      },
    },
  ],
};